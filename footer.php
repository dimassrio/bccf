		<footer class="footer">
			<nav class="footer-nav clearfix">
				<?php wp_nav_menu( array(
					'container' => false,
					'theme_location' => 'secondary' 
				) ); ?> 
			</nav>
			<p>
			    <a href="https://twitter.com/bccf_bdg">Twitter</a> &nbsp; <a href="https://www.facebook.com/BandungCreativeCityForum">Facebook</a>
			</p>
			<div class="copyright">Copyright &copy; <?php echo date('Y'); ?>. BCCF. All Rights Reserved.</div>
		</footer>
	
		<script src="<?php echo get_stylesheet_directory_uri(); ?>/assets/js/jquery-2.1.1.min.js"></script>
		<script src="<?php echo get_stylesheet_directory_uri(); ?>/assets/js/bootstrap.min.js"></script>
		<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+"://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>
		<?php wp_footer(); ?>
	</body>
</html>