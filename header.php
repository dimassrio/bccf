<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="<?php bloginfo( 'description' ); ?>">
	<meta name="author" content="<?php bloginfo( 'name' ); ?>">
    
	<title><?php wp_title( ' | ', true, 'right' ); ?></title>
	
	<link href="<?php echo get_stylesheet_directory_uri(); ?>/assets/css/bootstrap.min.css" rel="stylesheet">
	<link href='http://fonts.googleapis.com/css?family=Raleway:300,400,700|Open+Sans:300,400,700' rel='stylesheet' type='text/css'>
	<link href="<?php echo get_stylesheet_directory_uri(); ?>/assets/css/jquery.mCustomScrollbar.css" rel="stylesheet">
	<link href="<?php echo get_stylesheet_directory_uri(); ?>/assets/css/style.css" rel="stylesheet">
	<link href="<?php echo get_stylesheet_directory_uri(); ?>/style.css" rel="stylesheet">
	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>

<?php 
// if ( !is_front_page() ) {

if ( !is_page('home') && !is_page('gallery')) :
	$htmlheaderpage = "header-page";
	$htmlheader_style = "style='background:url(".get_stylesheet_directory_uri()."/assets/img/header.jpg) center; background-size:cover;'"; ?>
<header class="header <?php echo $htmlheaderpage;?>" <?php echo $htmlheader_style;?> >
<?php else: ?>
<header class="header">
<?php endif; ?>
	<?php if ( !is_page('home') && !is_page('gallery') ) { ?><div class="header-filter"></div><?php } ?>
	<nav class="navbar navbar-inverse<?php if ( is_front_page() ) { ?> navbar-fixed-top<?php } ?>" role="navigation">
		<div class="container">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#menu">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="<?php echo esc_url( home_url( '/' ) ); ?>"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/logo.png" alt="<?php bloginfo( 'name' ); ?>"></a>
			</div>

			
				<?php wp_nav_menu( array( 
					'container_class' => 'collapse navbar-collapse',
					'container_id' => 'menu',	
					'menu_class' => 'nav navbar-nav navbar-right', 
					'theme_location' => 'primary',
	                'fallback_cb'       => 'wp_bootstrap_navwalker::fallback',
	                'walker'            => new wp_bootstrap_navwalker())
				); ?> 
			
		</div><!-- /.container -->
	</nav>
<?php
	// if ( !is_front_page() ) {
if ( !is_page('home') && !is_page('gallery')) {
?>
	<div class="container">
			
			<?php if ( is_category() || is_archive() ) { ?>
                <h1 class="page-title"><?php single_cat_title('Archive for '); ?></h1>
            <?php } elseif (is_home()) { ?>
                <h1 class="page-title">News</h1>
            <?php } else { ?>
                <h1 class="page-title"><?php the_title(); ?></h1>
            <?php } ?>
	</div>
<?php } ?>
</header>
