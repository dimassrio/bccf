<?php
/**
 * Template Name: Front Page
 *
 */
?>

<?php get_header(); ?>

		<section id="myCarousel" class="carousel slide">
			<div class="carousel-inner">
				<?php global $post; $args=array( 'post_type'=> 'slider','posts_per_page'=>6  ); $posts = get_posts($args); foreach ( $posts as $post ) : setup_postdata( $post ); ?>
                <?php $thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );$url = $thumb['0'];?>

				<div class="item">
					<div class="fill" style="background-image:url('<?=$url?>');"></div>
					<div class="carousel-caption">
						<blockquote>
						    <?php the_content(); ?>
						</blockquote>
					</div>
				</div>
				
				<?php endforeach; wp_reset_postdata(); ?>
			</div><!-- /.carousel-inner -->

			<div class="filter"></div>

			<!-- Controls -->
			<a class="left carousel-control" href="#myCarousel" data-slide="prev">
				<img class="arrow-control left" src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/arrow-left.png" alt="arrow-left">
			</a>
			<a class="right carousel-control" href="#myCarousel" data-slide="next">
				<img class="arrow-control right" src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/arrow-right.png" alt="arrow-right">
			</a>
			

			
		</section><!-- /.carousel -->

		<div class="teaser-area container">
			<div class="row">
				<div class="col-sm-3">
					<a href="https://twitter.com/HelarFestBdg">
						<div class="teaser" style="background-color:#232425;"><!--custom bg-color-->
							<img class="teaser-thumbnail img-responsive center-block" src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/sponsor1.jpg" alt="Hellarfest">
						</div>
					</a>
				</div>
				<div class="col-sm-3">
					<a href="https://twitter.com/simpulinstitute">
						<div class="teaser" style="background-color:#d26b2d;"><!--custom bg-color-->
							<img class="teaser-thumbnail img-responsive center-block" src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/sponsor2.jpg" alt="Simpul Institute">
						</div>
					</a>
				</div>
				<div class="col-sm-3">
					<a href="https://twitter.com/simpulinstitute">
						<div class="teaser" style="background-color:#d26b2d;"><!--custom bg-color-->
							<img class="teaser-thumbnail img-responsive center-block" src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/sponsor2.jpg" alt="Simpul Institute">
						</div>
					</a>
				</div>
				<div class="col-sm-3">
					<a href="<?php echo site_url(); ?>/about/">
						<div class="teaser" style="background-color:#fff;"><!--custom bg-color-->
							<img class="teaser-thumbnail img-responsive center-block" src="http://inbigdeal.com/bccf/wp-content/uploads/2015/07/BANDUNG-CREATIVE-CITY.jpg" alt="Bandung Creative City">
						</div>
					</a>
				</div>
			</div>
		</div>

		
		<script src="<?php echo get_stylesheet_directory_uri(); ?>/assets/js/jquery-2.1.1.min.js"></script>
		<script src="<?php echo get_stylesheet_directory_uri(); ?>/assets/js/bootstrap.min.js"></script>
		<script>
			$('.carousel').carousel({
					interval: 5000 //changes the speed
			});
            $('.item:first').addClass('active');
		</script>
	</body>
</html>



 