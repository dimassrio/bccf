<?php get_header(); ?>
   <div class="container wrapper">
    <div class="row">
        <div class="col-md-7 content">
            <?php while ( have_posts() ) : the_post(); ?>

            <?php get_template_part( 'content' ); ?>

            <?php endwhile; ?>

        </div>
        <?php get_sidebar(); ?>
    </div>
</div>
<?php get_footer(); ?>