<article class="post">
    <header class="entry-header">
        <div class="entry-cover">
           <?php if ( is_single() ) { 
            the_post_thumbnail(); } else { ?>
            <a href="<?php the_permalink(); ?>">
                <?php the_post_thumbnail('full'); ?>
            </a>
            <?php } ?>
        </div>
        <h2 class="entry-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
        <div class="entry-meta">
            <span class="meta-date"><?php the_date('j F Y'); ?></span> under <span class="meta-category"><?php the_category(' '); ?></span>
        </div>
    </header>
    <div class="entry-content">
        <?php if ( is_single() ) { 
            the_content(); } else { 
            the_excerpt(); ?>
            <a class="readmore" href="<?php the_permalink(); ?>">Read more</a>
        <?php } ?>
    </div>
</article>